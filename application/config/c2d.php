<?php

$config['status']=array(
                                                '0'=>'Payment not received',
                                                '1'=>'Payment processed',
                                                '2'=>'Payment settled',
                                                '3'=>'Cancel',
                                                '4'=>'Expired'
                                           );

$config['s3Url']="http://c2dcdn.s3.amazonaws.com/";

$config['InvoiceGenerationMsgWithInvoicelink']="Hello  , Your Order with #%s  for Rs:%s/- has been generated.\n Kindly visit %s to view";
$config['InvoiceGenerationMsgWithoutInvoicelink']="Hello , Your Order with #%s  for Rs:%s/- has been generated.";

$config['bitlyaccesstoken']="ba3bbc4ff38b01ccec06b1679d9f17f85a5a1a91";

$config['bitlyUrl']="http://bit.ly/";

$config['cdnpaths']=array(
                                                    'testing'=>array(
                                                                                        'products'=>'testing/products',
                                                                                        'invoices'=>'testing/invoices'
                                                                                   ),
    
                                           'production'=>array(
                                                                                       'products'=>'production/products',
                                                                                        'invoices'=>'production/invoices'
                                                                                 )                               
                                                );

$config['GcmCredentials']  = array(
    'merchant'  => array(
        'gcm_url'           => 'https://android.googleapis.com/gcm/send', 
        'google_api_key'    => "AIzaSyDGxHufeW2uyiYyxz8BMOILVd5EWognhyg"
    ),
    'channel_partner'   => array(
        'gcm_url'           => 'https://gcm-http.googleapis.com/gcm/send',
        'google_api_key'    => "AIzaSyBk7XDeCYesfqrN57YCfAEaSmDMkOvcWBI"
    )
);

$config['developmentRetailers']=array('8108681401','9819042543','7101000450');