<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
          if(!$this->session->userdata('isLoggedIn')):

            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');

          endif;
    }
    
    public function index()
    {
        $this->load->view('dashboard/index');
    }
    
    public function testbitinfo()
    {
        $this->load->library('Curl');
        
        $link=$this->curl->get("https://api-ssl.bitly.com/v3/link/lookup",array('access_token'=>'ba3bbc4ff38b01ccec06b1679d9f17f85a5a1a91','url'=>'http://c2dcdn.s3.amazonaws.com/images/59df21c2b7a5c7de0187b8c25b4249de.png'));
        
        echo "<pre>";
        print_r($link);
        echo "</pre>";
        }
        
        public function testbitshorten()
        {
            
        $this->load->library('Curl');
        $path=$CI->config->item('is_live')?$c2dpaths['production'][$type]:$c2dpaths['testing'][$type];
        
        $link=$this->curl->get("https://api-ssl.bitly.com/v3/shorten/info",array('access_token'=>'ba3bbc4ff38b01ccec06b1679d9f17f85a5a1a91','longUrl'=>'http://c2dcdn.s3.amazonaws.com/images/a0fd18278c9e4430e76b79f1c22e6fe2.png'));
        
        echo "<pre>";
        print_r($link);
        echo "</pre>";
        }
}
