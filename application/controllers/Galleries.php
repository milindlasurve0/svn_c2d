<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Galleries extends CI_Controller {
    
    public function __construct() {
        
        parent::__construct();
        
        if(!$this->session->userdata('isLoggedIn')) {
            $this->session->set_flashdata('error','You need to be logged in to view that page');
            redirect('/');
        }
        
        $this->load->model('Gallery');
    }
    
    /*
     *  Multiple Gallery Images Uploading Form
     */
    public function index() {
        
        $this->load->view('gallery/image_upload');
    }
    
    /*
     *  Gallery Images Upload Backend
     */
    public function uploadGallery() {
        
        $image  = $_FILES['image_gallery'];
        $label  = $this->input->post('image_label');
        
        $res = $this->imageGalleryUpload($image, $label);           // image upload function
        
        if(!empty($res['failed_images'])) {
            $this->session->set_flashdata('size_format', implode(', ', $res['failed_images']) .
                    ' have not been uploaded because of different file format or size exceeded 1 MB');
        }
       
        if(!empty($res['data'])) {
            $this->Gallery->imageUpload($res['data']);
            $this->session->set_flashdata('success', 'Image Uploaded Successfully');
        } else {
            $this->session->set_flashdata('error', 'No Image Uploaded');
        }
        
        redirect('/galleries');
    }
    
    /*
     *  Image Upload function
     */
    public function imageGalleryUpload($image, $label) {
        
        $config['upload_path']      = './upload/wholesaler_gallery/';
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = '1024';
        $config['encrypt_name']     = true;
        
        $this->load->library('upload', $config);
        
        $this->upload->initialize($config);
        
        $data           = array();
        $failed_images  = array();
        
        for($i=0; $i<count($label); $i++) {
            
            foreach ($image as $key => $ig) {
                $_FILES['image_gallery'][$key] = $ig[$i];
            }
            
            if(!empty($_FILES['image_gallery']['name'])) {
                
                if($this->upload->do_upload('image_gallery')) {
                    
                    $data[$i]['client_id']  = $this->session->userdata['user']->id;
                    $data[$i]['caption']    = $label[$i];
                    $data[$i]['filename']   = $this->upload->file_name;
                    
                    moveToS3($this->upload->data(),"products");
                } else {
                    
                    $failed_images[] = $_FILES['image_gallery']['name'];
                }
            }
        }
        
        $res['failed_images']   = $failed_images;
        $res['data']            = $data;
        
        return $res;
    }

    /*
     *  Viewing All Gallery Images
     */
    public function lists() {
        
        $data['data'] = $this->Gallery->viewImages($this->session->userdata['user']->id);
        
        $this->load->view('gallery/index', $data);
    }
}