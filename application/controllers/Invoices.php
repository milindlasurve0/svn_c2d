<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends CI_Controller
{
    
    var $upload_errors=array();
    var $file_name="";
    var $bitlyHash="";
    
    public function __construct()
    {
        parent::__construct();
        
        if(!$this->session->userdata('isLoggedIn')):

          $this->session->set_flashdata('error','You need to be logged in to view that page');
        
                if($this->input->is_ajax_request()):

                          echo json_encode(array('status'=>true,'type'=>false,'message'=>'Session expired'));exit();

                 else:

                           redirect('/');

                endif;

        endif;
          
        $this->load->model('Invoice');
    
    }
    
    public function generate()
    {
         if($this->input->post()):
            
             $this->load->library('form_validation');
           
           // $this->form_validation->set_rules('client_ref_id', 'Invoice Id', 'required|trim|max_length[250]|callback_checkRefId');
            $this->form_validation->set_rules('mobile', 'Mobile', 'required|trim|is_numeric|max_length[10]|min_length[10]');
            $this->form_validation->set_rules('amount', 'Amount', 'required|trim|is_numeric|max_length[10]|integer');
            $this->form_validation->set_rules('expiry_datetime', 'Expiry Time', 'required|trim|callback_checkexpirydatetime');
            $this->form_validation->set_rules('ref_description', 'Description', 'trim|max_length[250]');
            $this->form_validation->set_rules('invoice_note', 'Note', 'trim|max_length[250]');
            //$this->form_validation->set_rules('invoice_file', 'Invoice file', 'callback_validate_upload');
            
           if ($this->form_validation->run() == FALSE): 
                    
                       $this->session->set_flashdata('error','There was error while creating Invoice');

                       $this->load->view('invoices/generate');
                       
               else:
                            $postdata=$this->input->post();
               
                            if(!$this->uploadInvoice()):

                                                   $this->session->set_flashdata('error',  $this->upload_errors);

                                                   $this->load->view('invoices/generate');

                                                   return;

                             endif;   

                            $postdata['file_name']=  $this->file_name;
                            $postdata['bitlyhash']=  $this->bitlyHash;
                            
                            if($insert_id=$this->Invoice->create($postdata)):

                                   $this->sendInvoiceSms($postdata);
                            
                                   $this->session->set_flashdata('success','Invoice was generated successfully with #'.$insert_id);

                                   redirect('invoices/generate');

                            else:

                                    $this->session->set_flashdata('error','There was error while inserting record');

                            endif;

                     
                     
               endif;
                
       else:
           
            $this->load->view('invoices/generate');
       
       endif;
       
    }

 function validate_upload()
    {
    
        if(empty($_FILES['invoice_file']['name'])):
                $this->form_validation->set_message('validate_upload', 'Kindly upload your invoice');  
                return false;
          else:
                return true;
          endif;
     }
 
  function checkexpirydatetime($expiry_time)
     {
         if(strtotime($expiry_time)>=strtotime('today')):
             return true;
         endif;
         
          $this->form_validation->set_message('checkexpirydatetime', 'The %s field Should be greater than current or equal to current day ');  
         return false;
     }
     
     function checkRefId($client_ref_id)
     {
         if($this->Invoice->IsUniqueRefid($client_ref_id)):
             return true;
         endif;
         
          $this->form_validation->set_message('checkRefId', 'Order no is already used.');  
         return false;
     }
     
     function  listall()
     {
         $invoices=$this->Invoice->getAll();
         
         $data['invoices']=$invoices;
         
         $this->load->view('invoices/listall',$data);
     }
     
     function reject()
     {
         if($this->input->is_ajax_request()  && $this->input->post()):
             
                $id=$this->input->post('id');
                if($this->Invoice->isVaildCancelRequest($id)):
                        if($this->Invoice->updateStatusToReject($id)):
                                 echo json_encode(array('status'=>true,'type'=>true,'message'=>'Success'));
                        else:
                                 echo json_encode(array('status'=>true,'type'=>false,'message'=>'Unable to update status'));
                        endif;
               else:
                    echo json_encode(array('status'=>true,'type'=>false,'message'=>'This Invoice cannot be rejected.'));
               endif;

         else:
             die("Invalid Request");
         endif;
     }
     
     public function test()
     {
      echo   decryptInvoiceId("Xc-1454415366");
     }
     
     public function uploadInvoice()
     {
          if(!empty($_FILES['invoice_file']['name'])):
              
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']     = '1024';
                $config['encrypt_name']=true;
                
                $this->load->library('upload', $config);
                
                $this->upload->initialize($config);

                if ($this->upload->do_upload('invoice_file')):
                     $this->file_name=$this->upload->data('file_name');     
                     $this->bitlyHash=  $this->getBitLyUrl($this->file_name);
                     moveToS3($this->upload->data(),'invoices');
                     return true;
               else:
                    $this->upload_errors= $this->upload->display_errors();
                    return false;
               endif;
               
          else:
               
               return true;
           
           endif;    
             
     }
     
     public function sendInvoiceSms($params)
     {
         $this->load->library('Curl');
          
            if(!empty($this->file_name)): //Send invoice link in SMS if invoice was  uploaded .
                  $sms=  sprintf($this->config->item('InvoiceGenerationMsgWithInvoicelink'),(string)$params['client_ref_id'],(string)$params['amount'],(string)$this->config->item('bitlyUrl').$this->bitlyHash);
            else:
                   $sms=  sprintf($this->config->item('InvoiceGenerationMsgWithoutInvoicelink'),(string)$params['client_ref_id'],(string)$params['amount']);
           endif;
           
            $this->curl->get("http://www.smstadka.com/redis/insertInQsms",array('mobile'=>$params['mobile'],'root'=>'tata','sms'=>$sms));
         
     }
     
     public function getBitLyUrl($filename)
     {
          $this->load->library('Curl');
          
          $cdnpaths=  $this->config->item('cdnpaths');
          
          $path=$this->config->item('is_live')?$cdnpaths['production']['invoices']:$cdnpaths['testing']['invoices'];
          
          $res=$this->curl->get("https://api-ssl.bitly.com/v3/shorten/info",array('access_token'=>  $this->config->item('bitlyaccesstoken'),
                                                                                                                                                'longUrl'=>  $this->config->item('s3Url').$path."/".$filename )
                                                     );
          if(!empty($res)):
            return $res->data->hash;
          endif;
          
          return ;
        
     }
}
