<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
    var $upload_errors=array();
    var $file_names=array();
    
    public function __construct()
    {
        parent::__construct();
        
        if(!$this->session->userdata('isLoggedIn')):

          $this->session->set_flashdata('error','You need to be logged in to view that page');
        
                if($this->input->is_ajax_request()):

                          echo json_encode(array('status'=>true,'type'=>false,'message'=>'Session expired'));exit();

                 else:

                           redirect('/');

                endif;

        endif;
          
        $this->load->model('Product');
    
    }
    
    public function post()
    {
        $data['categories']=  $this->Product->getPostCategories();
        
        if($this->input->post()):
            
                $this->load->library('form_validation');
        
                 $this->form_validation->set_rules('title', 'Title', 'trim|max_length[150]|required');
                 $this->form_validation->set_rules('group_id', 'Category', 'trim|required|is_numeric');
                 $this->form_validation->set_rules('description', 'Description', 'trim|max_length[500]|required');
                 $this->form_validation->set_rules('price', 'Price', 'trim|max_length[100]');
              
                 if ($this->form_validation->run() == FALSE): 
                       
                        $this->session->set_flashdata('error','There was error while posting product');

                        $this->load->view('products/post',$data);
                       
                  else:
                      
                       $postdata=  $this->input->post();
                  
                        if(!$this->uploadProduct()):

                                                   $this->session->set_flashdata('error',implode(",",$this->upload_errors));

                                                    $this->load->view('products/post',$data);

                                                   return;

                        endif;   
                             
                        $postdata['product_file']=  $this->file_names;
                        
                        if($postId=$this->Product->create($postdata)):
                            
                                    $this->session->set_flashdata('success','Your posting was successful');

                                    $this->sendNotifications($postId);
                                    
                                    redirect('products/post');
                                   
                          else:

                                    $this->session->set_flashdata('error','There was error while while posting');

                            endif;
                        
                  endif;
                  
       else:
            
               $this->load->view('products/post',$data);
        
       endif;
     
    }
    
    
     public function uploadProduct()
     {
         if(!empty($_FILES['product_file']['name'])):
             
                $files=$this->getImageAsArray($_FILES);
         
                $config['upload_path'] = './upload/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']     = '1024';
                $config['encrypt_name']=true;
                
                $this->load->library('upload', $config);
                
                $this->upload->initialize($config);

                foreach($files as $file):
                    $_FILES=$file;
                       if ($this->upload->do_upload('product_file')):
                            $this->file_names[]=$this->upload->data('file_name'); 
                             moveToS3($this->upload->data(),'products');
                       else:
                            $this->upload_errors[]= $this->upload->display_errors();
                       endif;
               endforeach;
               
               if(empty($this->upload_errors)):
                   return true;
               else:
                   $this->removeUploadedFiles();
                   return false;
               endif;
               
           else:
               
               $this->upload_errors[]="You must upload atleast one image";
               return false;
           
           endif;    
             
     }
     
     public function listall()
     {
         $posts=$this->Product->getAllPostsBelongingtoWs();
         
         $data['posts']=array();
         
         if(!empty($posts)):
             
                $posts=  $this->mapImagesToPosts($posts);

                $data['posts']=$posts;
         
         endif;
         
         $this->load->view('products/listall',$data);
     }
     
     public function mapImagesToPosts($posts)
     {
         $postsIds=array();
         
         foreach($posts as $value): $postsIds[]=$value['id']; endforeach;
         
         $images=  $this->Product->getImagesByPostIds($postsIds);
         
         foreach ($posts as $key=>$post):
                    $posts[$key]['images']=isset($images[$post['id']])?$images[$post['id']]:array();
         endforeach;
         
         return $posts;
      }
    
    public function updatePost() {
        
        $post_id = $this->input->post('post_id');
        
        $newpost_id = $this->Product->updatePost($post_id);
        
        $this->sendNotifications($newpost_id);
        
        echo json_encode(array('status'=>'done'));
    }
     
     public function getImageAsArray($files)
     {
         $totalfiles=count($files['product_file']['name']);
         $imgs=array();
         
         for($i=0;$i<$totalfiles;$i++):
        
             $imgs[$i]['product_file']=array(
                                                                        'name'=>$files['product_file']['name'][$i],
                                                                        'type'=>$files['product_file']['type'][$i],
                                                                        'tmp_name'=>$files['product_file']['tmp_name'][$i],
                                                                        'error'=>$files['product_file']['error'][$i],
                                                                        'size'=>$files['product_file']['size'][$i],
                                                                      );
         endfor;
         
         return $imgs;
     }
     
     public function removeUploadedFiles()
     {
         foreach ($this->file_names as $file):
             unlink('./upload/'.$file);
             removeFromS3($file,'products');
         endforeach;
     }
     
     public function remove()
     {
            removeFromS3('4d501631d7318fbcb18090e12bdde718.png','products');
     }
     
     public function sendNotifications($postId)
     {
         $post=$this->Product->findPostById($postId);
    
         if(!empty($post)):  
             
                       $post=  $this->mapImagesToPosts($post);

                       $this->load->library('Gcm');

                        $payload = json_encode(array(

                                  "type" => "C2dFeed",
                                  "title" => "New Feed",
                                  "msg" => $post,
                                  "newMsg"=>''
                              ));

                          $wrapper = array(
                                "data" => $payload,
                              "time_to_live"=>'86400'
                            );

                          $RetailerIds= $this->getValidRetailers($post[0]['client_id']);
                          
                          if(!empty($RetailerIds)):
              
                               $batches=  createBatch($RetailerIds,999);

                               foreach($batches as $batch):

                                      $res=$this->gcm->send_notification(
                                                                                                            $batch,
                                                                                                             $wrapper
                                                                                                           );
                                                                                                        
                              endforeach;
                               
                         endif;
          endif;
     }
     
     public function getValidRetailers($client_id)
     {
         $ids=array();
         
         $this->load->model('Shop');
         
         if($client_id=='10006'):
                $ids=$this->Shop->getTestingRetailers();
         else:
                $ids=$this->Shop->getValidRetailers();
         endif;
         
         return $ids;
     }
    
   
     
}
