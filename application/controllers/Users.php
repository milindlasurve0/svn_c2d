<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
    public function login()
    {
        
            if($this->input->post()):
                    
                        $this->load->model('user');
                        $data=$this->input->post();
                        
                        if($result=$this->user->authenticate($data)): 

                            $this->session->set_userdata(array('user'=>$result,'isLoggedIn'=>true));

                            redirect('dashboard');

                        else:

                            $this->session->set_flashdata('error','Invalid Username/Password combination');

                            redirect('/');

                        endif;
                
            endif;
            
        $data['csrf'] = array(
                                                    'name' => $this->security->get_csrf_token_name(),
                                                    'hash' => $this->security->get_csrf_hash()
                                            );
        
        $this->load->view('users/login',$data);
        
    }
    
      public function logout()
    {
         $this->session->set_userdata('isLoggedIn' , false);
         
         redirect('/');
    }
  
}
