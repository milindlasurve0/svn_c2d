<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller
{
    public function __construct() {
          parent::__construct();
         $this->load->model('api/Category');
    }
    
    public function listall()
    {
            if($this->input->method()=="post"):

                    $data['categories']=  $this->Category->getAllCategories();
                    
                     echo json_encode(array('status'=>'success','type'=>true,'data'=>$data));
                
            else:
                    echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800'));
            endif;

            exit();
    }
}
