<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller
{
    public function getlog($lines=50,$date=null,$type="php",$searchexp="",$nos=0)
  {
      if(is_null($date)):
          $date=date('Y-m-d');
      endif;
      
       $path=  trim(shell_exec("pwd"));
       
       $searchexp=  urldecode($searchexp);
       
      if($type=="php"):
        $command_string="tail -n$lines $path/application/logs/log-$date.php ";
      else:
        $command_string="tail -n$lines $path/application/logs/$type-$date.log  ";   
      endif;
      
      if(trim($searchexp)!=""):
           $command_string.=" | grep '$searchexp'  ";
      endif;
      
      if($nos>0):
          $command_string.=" | tail -$nos ";
      endif;
      
      echo $command_string;
      echo "<br/>";
      echo "<br/>";
      
     $output=  shell_exec($command_string);
      
      echo "<pre>";
      print_r($output);
      echo "</pre>";
      
      exit();
        
  }
  
  public function testing()
  {
        logerror("hit me","apisRequests");
  }
  
  public function saveTrackingData()
  {
      //$data='[{"retailerID":"4","postId":"5"},{"retailerID":"4","postId":"5"}]';
      
       if($this->input->method()=="post"):
             
                  logerror("Request Params for saveTrackingData: [".json_encode($this->input->post())."]","apisRequests");
       
               $data=json_decode($this->input->post('payload'),true);

               if(!empty($data)):

                    $this->load->model('api/Post');

                    $this->Post->setPostsLog($data);

                    echo json_encode(array('status'=>'success','type'=>true,'msg'=>'Sync Done'));
                
                else:
                    
                    echo json_encode(array('status'=>'success','type'=>false,'msg'=>'Invlalid Payload'));
                
                 endif;   
      
      
        else:
                  echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800'));
       endif;
      
  }
}

