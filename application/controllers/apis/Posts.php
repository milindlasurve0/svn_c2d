<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller
{
   public $retailerId=0;
  
    public function __construct() {
          parent::__construct();
         $this->load->model('api/Post');
    }
    
    public function find()
    {
        if($this->input->method()=="post"):
            
                $postid=  $this->input->post('postId');

                $this->retailerId= $this->input->post('retailerID')?$this->input->post('retailerID'):"0";

                logerror("Request Params for find: [".json_encode($this->input->post())."]","apisRequests");
                
                if(isValidInteger($this->retailerId)):
                    
                        $posts=  $this->Post->getPosts($postid,$this->retailerId);

                        $posts=  $this->__mapImagesToPosts($posts);

                        echo json_encode(array('status'=>'success','type'=>true,'data'=>$posts));
                
                else:
                    
                         echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'806'));
                
                endif;
               
       else:
                  echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800'));
       endif;
        
        exit();
    }
    
    public function __mapImagesToPosts($posts)
     {
        if(empty($posts)): return; endif; 
        
         $postsIds=array();
         
         foreach($posts as $value): $postsIds[]=$value['id']; endforeach;
         
         $images=  $this->Post->getImagesByPostIds($postsIds);
        
         foreach ($posts as $key=>$post):
                    $posts[$key]['images']=isset($images[$post['id']])?$images[$post['id']]:array();
         endforeach;
         
         return $posts;
      }
    
      public function interests()
      {
          if($this->input->method()=="post"):
               
               $postid=  $this->input->post('postId');
               $retailerId=  $this->input->post('retailerID');
               $shareFlag=  $this->input->post('can_share_mobile');
               $message=  $this->input->post('message');

                logerror("Request Params  for Interests: [".json_encode($this->input->post())."]","apisRequests");
                
                if(isValidInteger($postid) && isValidInteger($retailerId)):
                    
                         if($this->Post->setInterest(array('post_id'=>$postid,'retailer_id'=>$retailerId,'can_share_my_number'=>$shareFlag,'message'=>$message))):
                               echo json_encode(array('status'=>'success','type'=>true,'msg'=>'Request Sent'));
                         else:
                               echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'805','errorMsg'=>'Already Interested'));
                         endif;
                         
                 else:
                         echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'806','errorMsg'=>'Invalid Request Params'));
                endif;         
                
          else:
              echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800','errorMsg'=>'Invalid Request'));
          endif;
        
        exit();
            
      }
      
      //http://c2d.pay1.com/apis/posts/clickToCall?retailermobile=9221770571&wsmobile=8082011232&postId=5&wholesaler_id=1006&retailerID=856
      //http://c2d.pay1.com/apis/posts/clickToCall?retailermobile=9221770571&wsmobile=8082011232&postId=5&
      public function clickToCall()
      {
            if($this->input->method()=="post"):
                
                   logerror("Request Params for clickToCall  : [".json_encode($this->input->post())."]","apisRequests");
            
                   if($this->input->post('directCallHit')=="1"):   $this->Post->setCallLog($this->input->post());  echo json_encode(array('status'=>'success','type'=>true));exit(); endif;
                   
                    $retailerMobile=  $this->input->post('retailermobile');
                    $wsMobile=  $this->input->post('wsmobile');
                    $postID=  $this->input->post('postId');
                    $wsID=  $this->input->post('wholesaler_id');
                    $retailerID=  $this->input->post('retailerID');
                    
                   $this->load->library('Curl');
                   
                   $wsMobile=  $this->Post->getWsMobileById($wsID);
                   
                   $res=$this->curl->get("http://click2call.ddns.net/retailor.php",array('mobile'=>$retailerMobile,'incoming_route'=>$wsMobile));
                   
                   $this->sendSms($this->input->post(),$wsMobile);
                   
                   if($res=="done"):
                         $this->Post->setCallLog($this->input->post());  
                          echo json_encode(array('status'=>'success','type'=>true,'msg'=>'You will receive call shortly'));
                    else:
                           echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'807','errorMsg'=>'The wholesaler is busy at the moment. Please wait for sometime and try again later'));
                    endif;

          else:
              echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800','errorMsg'=>'Invalid Request'));
          endif;
        
        exit();
      }
      
      
      public function sendSms($params,$wsMobile)
      {
        
          $this->load->model('Shop');
           
          logerror("Params : ".  json_encode($params),'sendctcsms');
          
          $retailerInfo=  $this->Shop->getRetailerInfoForSms($params['retailerID']);
          
          logerror("RetailerInfo : ".  json_encode($retailerInfo),'sendctcsms');
          
          if(!empty($retailerInfo)):
          
            $sms="You have received call from retailer  Name : '{$retailerInfo['name']}',Shop:'{$retailerInfo['shopname']}',Mobile:'{$params['retailermobile']}'  " ;

            $this->load->library('Redis');

            $redisObj=  $this->redis->connect();

            $data=array('sender'=>'','mobile'=>$wsMobile,'sms'=>$sms,'root'=>'shops');

            logerror("Data : ".json_encode($data),"sendctcsms");    
            
            $res=$redisObj->lpush('smstadka',  json_encode($data));
          
          endif;
          
      }
      
    public function sendDeletePostsNotifications($ids="")
    {
          //   if($this->input->method()=="post"):
                 
                           $ids=  empty($ids)?$this->input->post('ids'):$ids;
             
                          if(!empty($ids)):  
                              
                                        $this->load->library('Gcm');

                                        $payload = json_encode(array(

                                                 "type" => "C2dFeed",
                                                 "title" => "",
                                                 "msg" => array(),
                                                 "newMsg"=>'',
                                                 "delete_posts"=>$ids
                                             ));

                                         $wrapper = array(
                                               "data" => $payload,
                                               "time_to_live"=>'86400'
                                           );

                                           $this->load->model('Shop');

                                          $gcmIds=$this->Shop->getValidRetailers();

                                          if(!empty($gcmIds)):

                                                    $batches=  createBatch($gcmIds,999);

                                                    foreach($batches as $batch):

                                                          $res=$this->gcm->send_notification(
                                                                                                                                $batch,
                                                                                                                                 $wrapper
                                                                                                                               );
                                                    endforeach;

                                                       echo json_encode(array('status'=>'success','type'=>true));
                                                       
                                                       $this->Post->deletePostById($ids);

                                            endif;
                                
                                endif;
                          
//          else:
//              echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800','errorMsg'=>'Invalid Request'));
//          endif;
        
        exit();
    }
      
     
}
