<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ws extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('api/Wholesaler');
    }
    public function profile()
    {
        
        if($this->input->method()=="post"):
            
                        $wholeSalerId=  $this->input->post('wholeSalerId');
                        $retailerID=  $this->input->post('retailerID');
                        
                         logerror("Request Params for Profile: [".json_encode($this->input->post())."]","apisRequests");
                         
                        if(isValidInteger($wholeSalerId) && isValidInteger($retailerID)):

                                $data=  $this->Wholesaler->getProfileInfo($wholeSalerId);

                                $posts=  $this->Wholesaler->getPostsByWsId($wholeSalerId,$retailerID);

                                $posts=  $this->__mapImagesToPosts($posts);

                                $data['posts']=$posts;

                                $data['gallery']=  $this->Wholesaler->getGalleryImagesByWsId($wholeSalerId);
                                
                                $this->Wholesaler->setWsProfileLog($this->input->post());

                                echo json_encode(array('status'=>'success','type'=>true,'data'=>$data)); exit();

                            else:

                               echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'806'));

                        endif;

        else:
                  echo json_encode(array('status'=>'success','type'=>false,'errorCode'=>'800'));
       endif;
       
    }
    
    public function __mapImagesToPosts($posts)
     {
        $this->load->model('api/Post');
           
        if(empty($posts)): return array(); endif; 
        
         $postsIds=array();
         
         foreach($posts as $value): $postsIds[]=$value['postID']; endforeach;
         
         $images=  $this->Post->getImagesByPostIds($postsIds);
        
         foreach ($posts as $key=>$post):
                    $posts[$key]['images']=isset($images[$post['postID']])?$images[$post['postID']]:array();
         endforeach;
         
         return $posts;
      }
}

