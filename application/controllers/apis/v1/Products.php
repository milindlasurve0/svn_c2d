<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
    public function __construct() {
          parent::__construct();
         $this->load->model('api/v1/Product');
    }
    
    public function listall()
    {
            $products=  $this->Product->getProductApiData();

            if(!empty($products)):
                $response= json_encode(array('status'=>'success','type'=>true,'data'=>$products));
             else:
                 $response=json_encode(array('status'=>'success','type'=>false));
             endif;

            $this->output->set_content_type('application/json');

            echo  $this->input->get_post('callback',true)?$this->input->get_post('callback',true).'('.$response.')':$response;
             
            exit();

    }
}
