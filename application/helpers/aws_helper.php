<?php

function moveToS3($data,$type='products')
{
        $CI = & get_instance();
     
        $cdnpaths=$CI->config->item('cdnpaths');
        
        $path=$CI->config->item('is_live')?$cdnpaths['production'][$type]:$cdnpaths['testing'][$type];
        $CI->load->library('s3');
        $CI->s3->putObjectFile($data['full_path'], 'c2dcdn', "{$path}/{$data['file_name']}", s3::ACL_PUBLIC_READ);
    
     if($CI->config->item('is_live')===true):     
        unlink($CI->upload->data('full_path'));
    endif;
    
}

function removeFromS3($filename,$type='products')
{
        $CI = & get_instance();
     
        $cdnpaths=$CI->config->item('cdnpaths');
        
        $path=$CI->config->item('is_live')?$cdnpaths['production'][$type]:$cdnpaths['testing'][$type];
        $CI->load->library('s3');
       
        $CI->s3->deleteObject('c2dcdn',$path."/".$filename);
}

function getImageUrl($filename,$type='products',$tobeReturned=false)
{
    $CI = & get_instance();
    
     $cdnpaths=$CI->config->item('cdnpaths');
     
    $path=$CI->config->item('is_live')?$cdnpaths['production'][$type]:$cdnpaths['testing'][$type];
    
     // if($CI->config->item('is_live')===true):
            if(!$tobeReturned):
            echo $CI->config->item('s3Url')."{$path}/".$filename;
            else:
            return $CI->config->item('s3Url')."{$path}/".$filename;
            endif;
     // else:
        //    echo base_url('upload/'.$filename);
    //  endif;
      
}