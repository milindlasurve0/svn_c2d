<?php

function encryptInvoiceId($invoice_id)
{
    $encryptionarray=array(
                                                '0'=>'oZ',
                                                '1'=>'Rx',
                                                '2'=>'Xc',
                                                '3'=>'Ag',
                                                '4'=>'qY',
                                                '5'=>'fM',
                                                '6'=>'Li',
                                                '7'=>'Nd',
                                                '8'=>'Jb',
                                                '9'=>'hS'
    );
    
    $temp=array();
    
    $Invoice_id_arr=str_split($invoice_id);
    
    foreach($Invoice_id_arr as $value):
        $temp[]=$encryptionarray[$value];
    endforeach;
    
    return implode('',$temp)."-".strtotime('now');
    
 }
 
 function decryptInvoiceId($value)
 {
       $encryptionarray=array(
                                                '0'=>'oZ',
                                                '1'=>'Rx',
                                                '2'=>'Xc',
                                                '3'=>'Ag',
                                                '4'=>'qY',
                                                '5'=>'fM',
                                                '6'=>'Li',
                                                '7'=>'Nd',
                                                '8'=>'Jb',
                                                '9'=>'hS'
    );
       
    $temp=array();
     
     $parts=explode("-",$value);
     
     $idstr=str_split($parts[0],2);
     
     foreach ($idstr as $row):
                        $temp[]=array_search($row,$encryptionarray);
     endforeach;
     
     return implode('',$temp);
     
 }
 
 function createBatch($batcharra,$parts=50)
{
      $temp=array();

        if(!empty($batcharra)):
                $temp=array_chunk($batcharra,$parts);
        endif;

        return $temp;
}

