<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Gcm {
	
    private $CREDS;
    private $GCM_URL;
    private $GOOGLE_API_KEY;
    
    /*
     * Constructor
     * 
     * @access public
     * @param $ANDROID_APP
     * @array $GcmCredentials
     */
    public function __construct($ANDROID_APP = 'merchant') {
        
        $CI = & get_instance();
    	$this->CREDS = $CI->config->item('GcmCredentials');
        
    	$this->CREDS[$ANDROID_APP] && $this->GCM_URL        = $this->CREDS[$ANDROID_APP]['gcm_url'];
    	$this->CREDS[$ANDROID_APP] && $this->GOOGLE_API_KEY = $this->CREDS[$ANDROID_APP]['google_api_key'];
    }

    /*
     * Sending Push Notification
     * 
     * @access public
     * @array $registatoin_ids
     * @param $message
     */
    public function send_notification($registration_ids = array(), $message) {
              
        $fields = array(
            'registration_ids'  => $registration_ids,
            'data'              => $message,
        );
 
        $headers = array(
            'Authorization: key=' . $this->GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->GCM_URL);
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3B48b Safari/419.3');
		
        // Execute post
        $result = curl_exec($ch);
        
        return $result;

        $response = array();
        if ($result === FALSE || empty($result)) {
            $response['status']         = "failure";
            $response['error_code']     = curl_errno($ch);
            $response['description']    = curl_error($ch);
            curl_close($ch);
            
            return $response;
        } else {
            curl_close($ch);
        	
            $result = json_decode($result, true);
            
            if($result['success'] == '0'){
                $response['status'] = "failure";
            } else {
                $response['status'] = "success";
            }
            $response['error_code']     = "";
            $response['description']    = $result;
            
            return $response;
        }
    }
}

?>