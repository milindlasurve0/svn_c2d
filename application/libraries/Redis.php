<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."third_party/predis/Autoloader.php";

class Redis
{
     var $ci;
     var $redisObj;
    
     public function __construct() 
     {
         $this->ci =& get_instance();
     }
     
     public function connect($serverKey='default')
     {
                Predis\Autoloader::register();

                $redis_servers = $this->ci->config->item('redis_servers');

               $credentials=$redis_servers[$serverKey];

               try
             {
                  $this->redisObj = new Predis\Client(array(
                                                                                                 'host' =>$credentials['host'],
                                                                                                  'port' =>$credentials['port'],
                                                                                                  'password'=>$credentials['password']
                                                                                                ));
             } 
               catch (Exception $e) 
             {
                $this->redisObj = false;
                 echo $e->getMessage();
                 echo "Couldn't connected to Redis";
                 logerror($e->getMessage(),"redisconnect");
             }

             return $this->redisObj;
        
     }
     
     
}

