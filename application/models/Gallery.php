<?php

class Gallery extends CI_Model {
    
    public function imageUpload($data) {
        
        $this -> db -> insert_batch('c2d_gallery', $data);
    }
    
    public function viewImages($client_id) {
        
        $this -> db -> select('*');
        $this -> db -> from('c2d_gallery');
        $this -> db -> where('client_id', $client_id);
        
        return $this -> db -> get() -> result_array();
    }
}