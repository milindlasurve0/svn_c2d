<?php

class Invoice extends CI_Model
{
    public function create($data)
    {
        $insert_array=array(
                                                   'mobile'=>$data['mobile'],
                                                   'amount'=>$data['amount'],
                                                   'in_date'=>date('Y-m-d'),
                                                   'intime'=>date('Y-m-d H:i:s'),
                                                   'expiry_time'=>$data['expiry_datetime']." 23:59:59 ",
                                                  //'client_ref_id'=>$data['client_ref_id'],
                                                   'cash_client_id'=>  getLoggedInUserId(),
                                                   'name'=>$data['name'],
                                                   'ref_description'=>$data['ref_description'],
                                                   'invoice_note'=>$data['invoice_note'],
                                                   'invoice_filename'=>$data['file_name'],
                                                   'bitlyhash'=>$data['bitlyhash']
                                            );
        
                                   
        if($this->db->insert('cash_payment_txn',$insert_array)):
              $insert_id=$this->db->insert_id();  
              $this->updateReferenceId($insert_id);
              return $insert_id;
        endif;
        
        return false;
        
    }
    
    
    function updateReferenceId($insert_id)
    {
        $this->db->update('cash_payment_txn',array('client_ref_id'=>$insert_id),array('id'=>$insert_id));
    }
    
    public function getAll()
    {
        $user_id=  getLoggedInUserId();
        
        $sql="SELECT cpt . * "
                . " FROM cash_payment_txn cpt  "
                . "WHERE cpt.cash_client_id ={$user_id} "
                . "ORDER BY cpt.updated_time DESC";
                
                     //. " ORDER BY FIELD(STATUS , '1' ) DESC ";
        
        $query=$this->db->query($sql);
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return;
        
        
    }
    
    public function isVaildCancelRequest($client_ref_id)
    {
        //$actualid=  decryptInvoiceId($encrypted_id);
        
        $query=$this->db->get_where('cash_payment_txn',array('client_ref_id'=>$client_ref_id,'cash_client_id'=>getLoggedInUserId()));
        
        if($query->num_rows()):
            
            $data=$query->row_array();
        
            if($data['status']=='0'):
                return true;
            else:
                return false;
            endif;
            
        endif;
        
        return false;
    }
    
    public function updateStatusToReject($client_ref_id)
    {
         if($this->db->update('cash_payment_txn',array('status'=>3),array('client_ref_id'=>$client_ref_id,'cash_client_id'=>  getLoggedInUserId()))):
             return true;
         endif;
         
         return false;
    }
    
    public function IsUniqueRefid($client_ref_id)
    {
        $query=$this->db->get_where('cash_payment_txn',array('client_ref_id'=>$client_ref_id,'cash_client_id'=>  getLoggedInUserId()));
        
         if(!$query->num_rows()):
             return true;
         endif;
         
         return false;
    }
}