<?php

class Product extends CI_Model
{
     public function create($data)
    {
        $insert_array=array(
                                                   'client_id'=>  getLoggedInUserId(),
                                                   'title'=>$data['title'],
                                                   'description'=>$data['description'],
                                                   'post_time'=>date('Y-m-d H:i:s'),
                                                   'post_date'=>date('Y-m-d'),
                                                   'category_id'=>$data['group_id']
                                            );
        
                                   
        if($this->db->insert('c2d_posts',$insert_array)):
                    return $this->saveImages($data['product_file'],$this->db->insert_id());
        endif;
        
        return false;
        
    }
    
    public function saveImages($imgs,$postID)
    {
        foreach($imgs as $img):
                        $this->db->insert('c2d_posts_images',array('post_id'=>$postID,'post_filename'=>$img));
        endforeach;
        
        return $postID;
    }
    
    public function getAllPostsBelongingtoWs()
    {
        $user_id=getLoggedInUserId();
        
        $sql="Select c2d_posts.*, c2d_categories.group_name from c2d_posts LEFT JOIN c2d_categories ON (c2d_categories.id = c2d_posts.category_id) where c2d_posts.client_id={$user_id} order by c2d_posts.post_time  DESC ";
        
         $query=$this->db->query($sql);
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return;
        
    }
    
    public function getImagesByPostIds($ids)
    {
       $temp=array();
       
       $this->db->select("post_id,post_filename");
       $this->db->where_in('post_id',$ids);
       $query=$this->db->get('c2d_posts_images');
       
       if($query->num_rows()):
                foreach($query->result_array() as $row):
                             $row['post_filename']=  getImageUrl($row['post_filename'],'products',$return=true);
                            $temp[$row['post_id']][]=$row['post_filename'];
                endforeach;
              return $temp;
        endif;
        
        return;
        
    }
    
    public function updatePost($post_id) {
        
        $post_time=date('Y-m-d H:i:s');
        
        $post_date=date('Y-m-d');
        
        $sql="Insert into c2d_posts (client_id,category_id,title,description,parent_id,post_time,post_date) "
                . "  SELECT client_id,category_id,title,description,'{$post_id}' as parent_id,'{$post_time}' as post_time,'{$post_date}' as post_date FROM c2d_posts WHERE id='{$post_id}' ";
        
         $query = $this->db->query($sql);
         
         $insert_id=$this->db->insert_id();
         
         $sql=" Insert into c2d_posts_images(post_id,post_filename)  "
                 . " SELECT '{$insert_id}' as post_id,post_filename from c2d_posts_images where post_id='{$post_id}' ";
         
         $query = $this->db->query($sql);         
        
         return $insert_id;
    }
    
    public function getPostCategories()
    {
        $sql = "Select * from c2d_categories";

        $query = $this->db->query($sql);

        if ($query->num_rows()):
            return $query->result_array();
        endif;

        return;
    }
    
    public function findPostById($id)
    {
        $sql="Select po.*,pc.company_name,pc.logo_url,pc.contact_no,pc.cover_photo "
                . " from c2d_posts po "
                . " JOIN  cash_payment_client pc "
                . " ON  po.client_id=pc.id "
                . " Where 1 AND  po.id='{$id}'  ";
     
        $query = $this->db->query($sql);

        if ($query->num_rows()):
            return $query->result_array();
        endif;

        return;
    }
}
