<?php

class Shop extends CI_Model
{
    private $shopDB;
    
    public function __construct() {
       $this->shopDB = $this->load->database('shops', TRUE);
    }
    
    public function getValidRetailers()
    {
        $past7Day=date('Y-m-d',  strtotime('-7 days'));
        
        $childsql="Select gcm_reg_id,user_id,SUBSTRING_INDEX( version, '_', -1 ) AS appversion "
                . " from user_profile "
                . " Where gcm_reg_id<>''  "
                . " AND DATE(updated) >= '{$past7Day}' AND device_type='android'  "
             //   . " AND user_id='31918718' order by updated DESC";
                . "  order by updated DESC";
                
        $sql="Select gcm_reg_id,appversion From ({$childsql}) as a  group by user_id HAVING appversion >= '5.0' "; 
       
       $query = $this->shopDB->query($sql);

        if ($query->num_rows()):
            
            foreach($query->result_array() as $row):
                    $temp[]=$row['gcm_reg_id'];
            endforeach;
            
            return $temp;
            
        endif;

        return;
    }
    
    public function getRetailerInfoForSms($id)
    {
        $sql="Select name,shopname,mobile from retailers where id='{$id}' ";
        
       $query = $this->shopDB->query($sql);

        if($query->num_rows()):
                return $query->row_array();
        endif;
        
        return;
    }
    
    public function getTestingRetailers()
    {
        $testMobileNos=$this->config->item('developmentRetailers');
        
        $inStmt=count($testMobileNos)>1?implode(',',$testMobileNos):$testMobileNos[0];
        
        $childsql="Select u.id as user_id,u.mobile,up.gcm_reg_id,SUBSTRING_INDEX( up.version, '_', -1 ) AS appversion "
                . " from users u  "
                . " JOIN user_profile up "
                . " ON u.id=up.user_id  AND u.mobile IN ({$inStmt}) "
                . " Where  device_type = 'android' "
                . " ORDER BY updated DESC ";
        
         $sql="Select gcm_reg_id,appversion From ({$childsql}) as a  group by user_id HAVING appversion >= '5.0' "; 
           
         $query = $this->shopDB->query($sql);

        if ($query->num_rows()):
            
            foreach($query->result_array() as $row):
                    $temp[]=$row['gcm_reg_id'];
            endforeach;
            
            return $temp;
            
        endif;

        return;
    }
}

