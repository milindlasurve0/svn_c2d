<?php

class Category extends CI_Model
{
    public function getAllCategories()
    {
        $sql="Select *  from c2d_categories";
        
         $query=  $this->db->query($sql);
        
        if($query->num_rows()):
                return $query->result_array();
        endif;
        
        return false;
        
    }
}

