<?php

class Post extends CI_Model
{
    public function getPosts($postid=0,$retailerID=0)
    {
        $sql="Select po.*,pc.company_name,pc.logo_url,if(is_still_interested is NULL,'0','1') as is_still_interested,pc.contact_no,pc.cover_photo "
                . " from c2d_posts po "
                . " JOIN  cash_payment_client pc "
                . " ON  po.client_id=pc.id  "
                . " LEFT JOIN c2d_posts_interests  pi "
                . " ON ( po.id = pi.post_id AND pi.retailer_id ={$retailerID} )  "
                . " Where 1 ";
        
        $sql.=!empty($postid)?" AND  po.id='{$postid}'  ":"";
        
         //$sql.= " order by id desc ";
         $sql.= " ORDER BY id DESC  LIMIT 50  ";
        
        $query=$this->db->query($sql);
        
        checkIfDbError('post/getPosts');
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return;
    }
    
     public function getImagesByPostIds($ids)
    {
       $temp=array();
       
       $this->db->select("post_id,post_filename");
       $this->db->where_in('post_id',$ids);
       $query=$this->db->get('c2d_posts_images');
       
       if($query->num_rows()):
                foreach($query->result_array() as $row):
                            $row['post_filename']=  getImageUrl($row['post_filename'],'products',$return=true);
                            $temp[$row['post_id']][]=$row['post_filename'];
                endforeach;
              return $temp;
        endif;
        
        return;
        
    }
    
    public function setInterest($params)
    {
        if($this->db->insert('c2d_posts_interests',array('post_id'=>$params['post_id'],
                                                                                                    'retailer_id'=>$params['retailer_id'],
                                                                                                    'can_share_my_number'=>$params['can_share_my_number']=="1"?"1":"0",
                                                                                                    'message'=>$params['message'],
                                                                                                     'interested_date'=>date('Y-m-d')
                                                                                                    )
                                            )):
            return true;
        endif;
      
        checkIfDbError('post/setFollowers');
        
        return false;
    }
    
    public function logDbError($source)
    {
        $error=$this->db->error();
        
        if($error['code']!=0 && !empty($error['message'])):
                    logerror("Source : ".$source,"apis");
                    logerror("Query : ".$this->db->last_query(),"apis");
                    logerror("Error : ".json_encode($this->db->error()),"apis");
        endif;
    }
    
    public function setCallLog($data)
    {
        $this->db->insert('c2d_click_to_call',array('post_id'=>$data['postId'],
                                                                                         'wholesaler_id'=>$data['wholesaler_id'],
                                                                                          'retailer_id'=>$data['retailerID'],
                                                                                          'retailermobile'=>$data['retailermobile'],
                                                                                          'wsmobile'=>$data['wsmobile'],
                                                                                          'call_timestamp'=>date('Y-m-d H:i:s'),
                                                                                          'call_date'=>date('Y-m-d')
                                                                                        )
                                         );
    }
    
    public function setPostsLog($data)
    {
       
        foreach($data as $row):
            $sql="Select * from c2d_post_logs where retailer_id='{$row['retailerID']}' AND post_id='{$row['postId']}'  ";
                    $query=  $this->db->query($sql);
                    if($query->num_rows()):
                        $this->db->where(array('retailer_id'=>$row['retailerID'],'post_id'=>$row['postId']));
                        $this->db->set('viewcount', 'viewcount+1', FALSE);
                        $this->db->set('updated',date('Y-m-d H:i:s'));
                        $this->db->set('log_date',date('Y-m-d'));
                        $this->db->update('c2d_post_logs');
                    else:
                        $this->db->insert('c2d_post_logs',array('retailer_id'=>$row['retailerID'],'post_id'=>$row['postId'],'viewcount'=>'1','log_date'=>date('Y-m-d'),'updated'=>date('Y-m-d H:i:s')));
                    endif;
        endforeach;
    }
    
    public function getWsMobileById($wid)
    {
        $sql="Select contact_no from cash_payment_client where id='{$wid}' ";
        
       $query=  $this->db->query($sql);
       
         if($query->num_rows()):
                    $result=$query->row_array();
                    return $result['contact_no'];
         endif;
         
         return ;
         
         
    }
    
    public function deletePostById($id)
    {
        $this->db->delete('c2d_posts',array('id'=>$id));
    }
}

