<?php

class Product extends CI_Model
{
    public function getProductApiData()
    {
        $sql = "select pa.*,c.group_name as category"
                . " from c2d_products_api pa "
                . "join c2d_categories c "
                . "on (c.id=pa.category_id) "
                . "order by pa.product_date"; 
        
        $query = $this->db->query($sql);
        
        if($query->num_rows()):
            return $query->result_array();
        endif;
        
        return false;
    }
}

