<div class="col-sm-5"  style="border: 1px solid black; min-height: 500px;">
   <script type="text/javascript" src="<?php echo base_url('public/js/moment.min.js'); ?>"></script>
    <div class="row">
    <div class="col-lg-5">
  		<img src="/public/images/pay1logo.png" style="width: 163px; height: 65px;">
    </div>
    <div class="col-lg-4 pull-right">
    <h1>Invoice</h1>; 
    </div>
  </div>
    
    <div class="row">
        <div class="col-lg-5">
          <div class="lh12 pull-left">
          <p>726 , Ijmima Complex</p>
          <p>Behind Infinity Mall</p>
          <p>Malad(West)</p>
           </div>
        </div>
        <div class="col-lg-7">
         <div class="lh12 pull-right row2">        
                <p><strong>Date : </strong><?php echo date("jS F Y");  ?></p>
                <p><strong>Pay1 Invoice Id : </strong> xxxx</p>
                <p><strong>Order No :  </strong><span id="row2custid"></span>  </p>
         </div>
       </div>
    </div>
    
    
    <div class="row">
  <div class="col-lg-6 lh12">
  <p class="billto">Bill To :</p>
  <p><strong>Name : </strong><span id="row3name">Payee Name</span></p>
  <p><strong>Mobile : </strong> <span id="row3mobile">Payee Mobile</span> </p>
  <p><strong>Expiry : </strong> <span id="row3expiry">Invoice Expiry time</span> </p>
  </div>
</div>
    
    
     <div class="row">
    <div class="col-lg-12 lh12">
        <table class="table table-bordered table-striped">
            <thead>
                <tr style="background-color: lightblue;">
                <th>Description</th>
                <th style="text-align: right;">Amount</th></tr>
            </thead>
            <tbody>
                <tr>
            <td id="row4desc" class="text-left mydescription">Item description</td>
            <td id="row4amount" class="text-right">Total payable amount in Rs</td>
                </tr>
            </tbody>
        </table>
    </div>
  </div>
    
    
    <div class="row">
        <div class="col-lg-6 lh12">
            <div class="panel">
                <div class="panel-heading othercomments">Other Notes</div>
                <div class="panel-body" id="row5mynotes">My Note</div>
            </div>
        </div>
    </div>
    
        <div class="row">
            <div class="col-lg-12 lh12 text-center">
                If you have any question about this invoice kindly contact <br/> c2d@pay1.in or call us at 09819032643
              <h3><i>Thankyou for your buisness !</i></h3>
            </div>
        </div>
</div>
