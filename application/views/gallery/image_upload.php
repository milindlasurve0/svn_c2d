<?php $this->load->view('common/header'); ?>

<div class="container">
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">    
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/gallery.js'); ?>"></script>
<?php $this->load->view('common/sidebar',array('gallery'=>true)); ?>
    
<div class="col-sm-5">

<form id="gallery_image" class="form-horizontal" method="post" action="/galleries/uploadGallery" enctype="multipart/form-data" >
<fieldset>
<legend>Gallery</legend>

<?php if ($this->session->flashdata('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?> 

<?php if ($this->session->flashdata('error')): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?> 

<?php if ($this->session->flashdata('size_format')): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error ! </strong><?php echo $this->session->flashdata('size_format'); ?>
    </div>
<?php endif; ?> 

<div id="multi_images">
    <div class="image">
        <div id="images_1">
            <div class="form-group form-group-sm">
                <label for="invoice_upload" class="control-label col-sm-4">Image Upload</label>
                <div class="col-sm-8">
                    <input type="file" id="gallery_images" name="image_gallery[]" />
                </div>
            </div>

            <div class="form-group form-group-sm">
                <label for="expiry_datetime" class="control-label col-sm-4">Image Label</label>
                <div class="col-sm-8">
                    <input type="text" id="label" name="image_label[]" class="form-control inpwidth250">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group form-group-sm">
    <label for="" class="control-label col-sm-3"></label>
    <div class="col-sm-9">
        <input type="submit" class="btn btn-default btn-sm" value="Upload" />
        <input type="button" class="add_image btn btn-default btn-sm" value="Add More" />
    </div>
</div>

</fieldset>
</form>
    
</div>

</div>  


<?php $this->load->view('common/footer'); ?>
