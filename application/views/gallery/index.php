<?php $this->load->view('common/header'); ?>

<div class="container">
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">
<style>
    
    .imagebox {
        float: left;
        margin: 0 30px 20px 0;
        min-height: 250px;
    }
    
    .caption-box {
        max-width: 250px;
        margin-top: 10px;
    }
    
    .image-box {
        border: 10px solid grey;
        max-width: 275px;
    }
    
</style>
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/gallery.js'); ?>"></script>
<?php $this->load->view('common/sidebar',array('gallery'=>true)); ?>
    
<div class="col-sm-10">
<legend>Gallery</legend>

<?php foreach($data as $record) { ?>
<div class="imagebox">
    <img src="/upload/wholesaler_gallery/<?php echo $record['filename']; ?>" class="image-box">
    <div class="caption-box"><?php echo $record['caption']; ?></div>
</div>
<?php } ?>
    
</div>

</div>  

<?php $this->load->view('common/footer'); ?>
