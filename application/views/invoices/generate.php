<?php $this->load->view('common/header'); ?>

<div class="container">
<link rel="stylesheet" media="screen" href="<?php echo base_url('public/css/bootstrap-datepicker.min.css'); ?>">    
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/invoice.js'); ?>"></script>
<?php $this->load->view('common/sidebar',array('invoice'=>true)); ?>
    
<div class="col-sm-5">

  <form class="form-horizontal" method="post" action="generate" enctype="multipart/form-data">
<fieldset>
<legend>Create Invoice</legend>

<?php if ($this->session->flashdata('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?> 

<?php if ($this->session->flashdata('error')): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?> 

<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <p><strong>Errors ! </strong><?php echo validation_errors('<p>', '</p>'); ?> </p>
  </div>
<?php endif; ?> 

<div class="form-group form-group-sm">
  <label for="mobile" class="control-label col-sm-3">Mobile</label>
  <div class="col-sm-9">
      <input autocomplete="off" class="form-control inpwidth250" id="mobile"  name="mobile"  placeholder="Mobile" required="" type="text" value="<?php echo set_value('mobile');  ?>" maxlength=10 minlength=10 />
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="amount" class="control-label col-sm-3">Amount</label>
  <div class="col-sm-9">
  <input  autocomplete="off"  class="form-control inpwidth90" id="amount"   name="amount"  placeholder="Amount" required="" type="text"  value="<?php echo set_value('amount');  ?>"  />
  </div>
</div>


<!--<div class="form-group form-group-sm">
  <label for="client_ref_id" class="control-label col-sm-3">Order No</label>
  <div class="col-sm-9">
      <input  autocomplete="off" class="form-control inpwidth250 showinputtip"   id="client_ref_id" name="client_ref_id" placeholder="Enter your order  id"  type="text"  value="<?php echo set_value('client_ref_id');  ?>"   />
  </div>
</div>-->

<div class="form-group form-group-sm">
  <label for="name" class="control-label col-sm-3">Name</label>
  <div class="col-sm-9">
  <input  autocomplete="off" class="form-control inpwidth250" id="name"   name="name"  placeholder="Reference name"  type="text" value="<?php echo set_value('name');  ?>"    />
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="ref_description" class="control-label col-sm-3">Description</label>
  <div class="col-sm-9">
      <textarea class="form-control inpwidth250" id="ref_description"   name="ref_description"  placeholder="Reference Description"  ><?php echo set_value('ref_description');  ?></textarea>
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="invoice_note" class="control-label col-sm-3">Note</label>
  <div class="col-sm-9">
  <textarea class="form-control inpwidth250" id="invoice_note"   name="invoice_note"  placeholder="My Note"  ><?php echo set_value('invoice_note');  ?></textarea>
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="expiry_datetime" class="control-label col-sm-3">Expiry Time</label>
  <div class="col-sm-9">
<!--  <input class="form-control inpwidth250" id="expiry_datetime"   name="expiry_datetime"  placeholder="Expiry Time " required="" type="text" />-->
  <div class="input-append date form_datetime">
      <input size="16" type="text"  name="expiry_datetime"  value="<?php echo set_value('expiry_datetime')?set_value('expiry_datetime'):date('Y-m-d',strtotime('+7 days'));  ?>"  id="expiry_datetime" class="form-control inpwidth250" readonly>
    <span class="add-on"><i class="icon-th"></i></span>
</div>
  </div>
</div>

<div class="form-group form-group-sm">
     <label for="invoice_upload" class="control-label col-sm-3">Upload</label>
     <div class="col-sm-9">
       <input id="my-file-selector" type="file" name="invoice_file" id="invoice_file">
       </div>
</div>


<div class="form-group form-group-sm">
    <label for="" class="control-label col-sm-3"></label>
  <div class="col-sm-9">
      <input type="submit" class="btn btn-default btn-sm" value="Generate payment" />
  </div>
</div>

</fieldset>
</form>
    
</div>

<?php //$this->load->view('common/preinvoice'); ?>
    
 </div>  


<?php $this->load->view('common/footer'); ?>
