<?php $this->load->view('common/header'); ?>

<div class="container">
<?php $this->load->view('common/sidebar',array('listall'=>true)); ?>
<script type="text/javascript" src="<?php echo base_url('public/js/listall.js'); ?>"></script>    
<div class="col-sm-10">


<fieldset>
<legend>Listings</legend>

<?php if(!empty($invoices)):  ?>

<table class = "table table-bordered">
   <thead>
      <tr>
         <th>ID</th>
         <th>Mobile</th>
         <th>Amount</th>
         <th>Intime</th>
         <th>Updated</th>
         <th>Note</th>
         <th>Desc</th>
         <th>Status</th>
         <th>Action</th>
      </tr>
   </thead>
   
   <tbody>
       
  <?php $status=$this->config->item('status');  ?>      
   <?php foreach($invoices as $invoice): ?>
      <tr>
         <td><?php echo $invoice['client_ref_id']; ?></td>
         <td><?php echo $invoice['mobile']; ?></td>
         <td><?php echo $invoice['amount']; ?></td>
         <td><?php echo $invoice['intime']; ?></td>
         <td><?php echo $invoice['updated_time']; ?></td>
         <td><?php echo $invoice['invoice_note']; ?></td>
         <td><?php echo $invoice['ref_description']; ?></td>
         <td><?php echo $status[$invoice['status']];  ?></td>
         <td><?php  if($invoice['status']=='0'): ?>
                <button class="btn btn-default btn-xs  btn-danger btnrejectinvoice"  data-id="<?php echo $invoice['client_ref_id'] ?>">Reject</button>
                    <?php endif;?>
         </td>
      </tr>
   <?php endforeach;  ?>   
   </tbody>
	
</table>

<?php else:  echo "No Invoices"; endif; ?>
</fieldset>

    
</div>
    
 </div> 

<?php $this->load->view('common/footer'); ?>