<?php $this->load->view('common/header'); ?>

<div class="container">
<?php $this->load->view('common/sidebar',array('postproduct'=>true)); ?>
<script type="text/javascript" src="<?php echo base_url('public/js/post.js'); ?>"></script>    
<div class="col-sm-5">

    <form class="form-horizontal" method="post" action="post" enctype="multipart/form-data" name="postproduct">
<fieldset>
<legend>Post product</legend>

<?php if ($this->session->flashdata('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Success ! </strong><?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?> 

<?php if ($this->session->flashdata('error')): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error ! </strong><?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?> 

<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <p><strong>Errors ! </strong><?php echo validation_errors('<p>', '</p>'); ?> </p>
  </div>
<?php endif; ?> 

<div class="form-group form-group-sm">
  <label for="title" class="control-label col-sm-3">Title</label>
  <div class="col-sm-9">
      <input autocomplete="off" class="form-control inpwidth250" id="title"  name="title"  placeholder="Title" required="" type="text" value="<?php echo set_value('title');  ?>" />
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="category" class="control-label col-sm-3">Category</label>
  <div class="col-sm-9">
      <select name="group_id"  id="group_id" required>
          <option value="">Select</option>
          <?php foreach($categories as $category): ?>
          <option value="<?php echo $category['id'] ?>"><?php echo $category['group_name']; ?></option>
          <?php endforeach; ?>
      </select>
  </div>
</div>

<div class="form-group form-group-sm">
  <label for="description" class="control-label col-sm-3">Description</label>
  <div class="col-sm-9">
      <textarea autocomplete="off" style="width: 513px; height: 164px;" class="form-control inpwidth250" id="title"  name="description"  placeholder="Product description" required="" ><?php echo set_value('description');  ?></textarea>
  </div>
</div>



<div id="addmoreimg">
<div class="form-group form-group-sm">
     <label for="product_file" class="control-label col-sm-3">Upload<p class="help-block">Size upto 1MB</p></label>
     <div class="col-sm-5">
         <input id="my-file-selector" type="file" name="product_file[]" id="product_file[]">
     </div>
     <div class="col-sm-2">
          <button type="button" class="btn btn-default btn-xs btn-addmore">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
     </div>
</div>
</div>



<div class="form-group form-group-sm">
    <label for="" class="control-label col-sm-3"></label>
  <div class="col-sm-9">
      <input type="submit" class="btn btn-default btn-sm" value="Post my product" />
  </div>
</div>

</fieldset>
</form>
</div>
 </div> 

<?php $this->load->view('common/footer'); ?>
