$(function(){
    
    $('button.btnrejectinvoice').on('click',function(){
        
               var self=$(this);
               
               var invoice_id=$(this).attr('data-id');
               
               var rejectinvoice=$.post(HOST+'invoices/reject',{id:invoice_id});
               
               rejectinvoice.done(function(res){
                   
                       res=$.parseJSON(res);
                       
                       if(res.status && res.type)
                       {
                                alert('Status updated to cancel');
                           
                                self.parent().prev('td').text('Cancel');
                                self.remove();
                                
                       }else
                       {
                           alert(res.message);
                           c2d.security.checkSession(res.message);
                       }
                       
               });
               
    });
    
})