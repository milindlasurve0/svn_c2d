$(document).ready(function(){
    
    $('form[name="postproduct"] button.btn-addmore').on('click',function(){
        
        var uploadHtml='<div class="form-group form-group-sm">\n<label for="product_file" class="control-label col-sm-3">Upload</label>\n<div class="col-sm-5">\n<input id="my-file-selector" type="file" name="product_file[]" id="product_file[]">\n</div>\n<div class="col-sm-2">\n<button type="button" class="btn btn-default btn-xs btn-removeme">\n<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>\n</button>\n</div>\n</div>';

        $('div#addmoreimg').append(uploadHtml);
        
    });
    
      $(document).on('click','form[name="postproduct"] button.btn-removeme',function(){
          
          $(this).parent().parent().remove();
          
      });
      
    $('.repost').click(function() {
        
        var post_id = $(this).data('id');
        
        if(confirm('Are You sure ?') == true) {
            
            $.post('/products/updatePost', {post_id: post_id}, function(e) { console.log("Done");  }, 'json');
            
            alert('Post Resubmited Successfully !!!');
        }
    });
    
    $('.deletepost').click(function() {
        
        var post_id = $(this).data('id');
        
        if(confirm('Are You sure ?') == true) {
            
            $.post('/apis/posts/sendDeletePostsNotifications', {ids: post_id}, function(e) {  console.log("Delete successfull "+post_id); }, 'json');
            
            alert('Post Deleted');
        }
    });
});